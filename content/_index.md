+++
+++

# Hi there! 👋

I'm Gregor Niehl, a tech enthusiast born in 2007 and located in Germany.
I'm interested in Linux and Open Source, and I code in my free time,
mainly related to the GNOME desktop environment.

My username is "**gregorni**" on most platforms.

I'm a member of the [GNOME Foundation](https://foundation.gnome.org/membership/)
and the [GNOME Circle Committee](https://wiki.gnome.org/CircleCommittee)

I write code in Python, Rust and Go, used to spend a lot of time
in Bash scripting, and have dabbled in Crystal a few times.

Apart from that, I also enjoy dancing.

## Apps

I write apps for the GNOME desktop. So far, they're mostly pretty goofy.

- [Letterpress](https://flathub.org/apps/io.gitlab.gregorni.Letterpress)

I also co-maintain a few other GNOME apps:

- [Upscaler](https://gitlab.gnome.org/World/Upscaler)
- [Workbench](https://github.com/workbenchdev/Workbench)

