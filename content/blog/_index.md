+++
title= "My blog"
sort_by="date"
template = "article_list.html"
page_template = "article.html"
+++

Here I occasionally (really occasionally) write about experiences I consider worth documenting.
