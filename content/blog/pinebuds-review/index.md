+++
title= "Review of the Pinebuds Pro"
date = 2023-01-21
[taxonomies]
tags = ["Audio", "Headphones", "Pine64"]
+++

I got the Pinebuds Pro for Christmas and here's my review on them.

![The box of the Pinebuds Pro](box.jpeg)

# About Pine64

In case you didn't know, the Pinebuds Pro are a pair of wireless earbuds by the company Pine64.
They are the company's first audio product. Apart from the earbuds,
Pine64 makes a lot of crazy and weird other products including Linux phones and tablets,
a laptop powered by the Arm-architecture (and not the more popular x86-architechture) and a soldering iron.

Like with their other products, Pine64 did something different with the Pinebuds Pro:
You can flash your own firmware on them. The firmware is the code preinstalled on a device.
On a pair of headphones, the firmware determines mainly how they sound,
as well as features like Active Noise Cancellation (ANC) and tap codes.
Most headphone manufacturers lock their firmware to avoid users breaking the headphones by accident.
Pine64 decided to make it “flashable”, meaning users can replace it by installing their own firmware.
I did not attempt this yet, partially because I'm fine with the default firmware,
but also because the two available custom firmwares,
[little-buddy](https://github.com/hall/little-buddy) and [OpenPineBuds](https://github.com/pine64/OpenPineBuds),
are (as of now) lacking some features, namely ANC.

# Unboxing

The Buds come in a neat little white box (picture above) containing the case with the buds themselves,
a charging cable (USB-A to USB-C), a smaller and a larger pair of eartips,
and a little booklet with instructions.

![The contents of the box](contents.jpeg)

# The case

The case is fairly hefty, and you will notice it if you put it in your pocket.
It is also quite large, just small enough to fit in my hand.
That space is filled by a battery that is advertised to last around 25 hours of charging the buds,
and even after a more two weeks of using them on and off I still haven't charged the case a single time.

![The case of the Pinebuds Pro](case.jpeg)

The case opens with a sliding mechanism, as described on top of the lid.
I must say I'm not a fan of that "Slide" label and icon,
I find it disturbs the balance of text on the lid and makes the whole case seem less sleek and minimalistic.

The case is smaller than it seems in the picture, around 3 cm tall perhaps (a bit more than an inch).
I was a bit worried that it would be weirdly tall, but those worries were unnecessary.

There's a series of 4 LEDs on the front of the case indicating charge status of the case as well as the buds,
depending on what's charging. These LEDs do their job well,
and it's easy to see how many of them are lit without having to look too closely.

# The Buds

As is the case with the case (pun intended), the buds are quite large. Unlike the case,
however, they are not very heavy given their size. They feel comfortable in your ear,
and although most people including me are probably fine with the default eartips,
the included additional pairs make sure the buds fit everyone well.

![The buds](buds.jpeg)

The branding on the buds is very subtle, with just a Pine64 logo on the touch surface.
In my eyes a perfect balance between making the buds feel like a Pine64 product and keeping them sleek and minimalist.

Although the touch surface feels like it will get grimy quickly,
it's actually surprisingly resistant to fingerprints.

The buds have a number of touch codes such as volume control and pause/play,
but using them is kind of hard as they can be hit or miss.
Sometimes I want to cycle through ANC modes (triple tap) and the music stops (double tap).
The single tap code, volume control, arguably the most important functionality,
works really well, though.

The LEDs on the buds don't fulfill any obvious purpose,
but they're still nice to have as a sign that the buds are active when you take them out of the case.

# Sound & ANC

The Buds have a surprising amount of bass, probably because of their size.
This won't bother you when watching content such as YouTube videos,
but you will notice it when listening to music. Whether you like it or not depends on your preferences.
For me personally, I didn't mind having more bass and quite liked the extra emphasis on the beat.
When listening to some songs on the Buds,
I was able to make out some basslines I had never noticed in them before,
which is a pro in my book.

The Buds have 4 ANC modes (on the default firmware): Standard ANC, Super ANC,
Ambient Mode and ANC off. Ambient Mode is frankly horrible,
because it distorts sound and makes a really annoying buzzing sound.
The Standard ANC is quite effective, basically blocking out all outside noise.
I just recently vacuumed the house while listening to music on Standard ANC mode and although I could hear the vacuum cleaner,
I nevertheless heard the music well and clearly. I can hardly hear any difference between Standard and Super ANC,
except Super ANC sounds more pronounced at low volume.
I noticed that both Standard and Super ANC make a slight low buzzing sound,
but it's not terribly noticeable,
especially considering you'll probably be using these ANC modes in environments where there's some background noise anyway.

# Price

The PineBuds Pro come in at an incredible <raw></raw>70 USD. The 2nd Generation Apple AirPods,
which the PineBuds Pro are often compared to in terms of features as well as sound quality,
come in at <raw>$</raw>130 USD (<raw>$</raw>60 more).
During my search for wireless earbuds I actually did not come across any other pair of wireless headphones under <raw>$</raw>100.
And even though the PineBuds Pro are much cheaper than their competitors,
they do not lack in functionality, features, build or sound quality.

# Conclusion

The PineBuds Pro are a pair of wireless earbuds, priced exceptionally well,
with very competitive features and sound quality,
making them a great choice for people who just want a simple way to listen to audio on their own.
They are also a great toy for tech nerds who want to tinker with their headphones and customize them to their liking.
The very reasonable price you pay for them supports a small company working towards a liberated and community-centric tech world.
I can thoroughly and with confidence recommend these earbuds to anyone.
